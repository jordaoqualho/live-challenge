import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "./Home";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />

          {/* Not Found Page Route */}
          {/* <Route path="*" component={NotFound} /> */}
        </Switch>
      </Router>
    </>
  );
}

export default App;
