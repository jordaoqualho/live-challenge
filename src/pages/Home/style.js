import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  position: relative;

  iframe {
    width: 100%;
    height: 100%;
    border: none;
  }

  .open_btn {
    background-color: var(--brown);
    position: absolute;
    border-radius: 50%;
    padding: 20px;
    bottom: 20px;
    left: 20px;
    transition: 0.3s ease;

    :hover {
      opacity: 0.5;
    }

    img {
      width: 30px;
    }
  }

  @media (max-width: 600px) {
    iframe {
      height: 35%;
    }
    .open_btn {
      display: none;
    }
  }
`;
