import React, { useState } from "react";
import { Container } from "./style";
import cabide from "img/cabide.svg";
import ShopList from "components/ShopList";

const Home = () => {
  const [openShopList, setOpenShopList] = useState(false);

  return (
    <Container>
      <iframe
        title="video"
        src="https://player.vimeo.com/video/639106275?h=ff6313a308&title=0&byline=0&portrait=0&autopause=0&app_id=122963"
      />
      <button
        className="open_btn"
        onClick={() => {
          setOpenShopList(true);
        }}
      >
        <img src={cabide} alt="" />
      </button>
      <ShopList setOpenShopList={setOpenShopList} openShopList={openShopList} />
    </Container>
  );
};

export default Home;
