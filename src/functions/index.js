export function tempAlert(msg, duration) {
  var el = document.createElement("div");
  var name = document.createElement("p");
  var text = document.createElement("p");

  el.setAttribute(
    "style",
    "position:fixed; opacity: 0 ;transition: 0.5s ease; top:0; right:0;padding: 10px 20px;  font-weight: 300;border-radius: 2px; background-color: #8d6742;font-size: 14px;  box-shadow: 5px 5px 25px rgba(0, 0, 0, 0.25);color: white;z-index:1000;"
  );

  name.setAttribute("style", "font-weight: 400;");
  text.setAttribute("style", "font-weight: 200;");

  name.innerHTML = msg;
  text.innerHTML = "foi adicionado ao carrinho!";

  el.appendChild(name);
  el.appendChild(text);

  // var node = document.createTextNode("This is new.");

  setTimeout(function () {
    el.style.top = "0";
    el.style.opacity = "1";
  }, 100);

  setTimeout(function () {
    el.style.opacity = "0";
    el.style.top = "-10%";
  }, duration);
  document.body.appendChild(el);
}
