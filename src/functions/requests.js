import api from "connection/index";
import { tempAlert } from "functions";

// ------------------------------ ESCOLA ------------------------------

export const doPostEndereco = async (
  escola,
  endereco,
  entidade,
  localDoArquivo
) => {
  const tempArray = escola;
  return await api
    .post("/enderecos", endereco)
    .then((res) => {
      tempArray.enderecoId = res.data.id;
      entidade === "professor"
        ? doPostProfessor(tempArray, localDoArquivo)
        : doPostEscola(tempArray, localDoArquivo);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doPostEndereco()`
      )
    );
};

export const doPostEscola = async (escola, localDoArquivo) => {
  return await api
    .post("/escolas", escola)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doPostEscola()`
      )
    );
};

export const doGetEscolas = async (
  termoDeBusca,
  setEscolas,
  setEstaCarregando,
  localDoArquivo
) => {
  return await api
    .get(`/escolas?nome=${termoDeBusca}`)
    .then((res) => {
      setEscolas(res.data);
      setEstaCarregando(false);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doGetEscolas()`
      )
    );
};

export const doGetUmaEscola = async (
  idDaEscola,
  setEscola,
  setEndereco,
  setEstaCarregando,
  localDoArquivo
) => {
  return await api
    .get(`/escolas/${idDaEscola}`)
    .then((res) => {
      setEscola(res.data);
      doGetEnderecoDaEscola(
        res.data.enderecoId,
        setEndereco,
        setEstaCarregando
      );
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doGetUmaEscola()`
      )
    );
};

export const doGetEnderecoDaEscola = async (
  enderecoId,
  setEndereco,
  setEstaCarregando,
  localDoArquivo
) => {
  return await api
    .get(`/enderecos/${enderecoId}`)
    .then((res) => {
      setEndereco(res.data);
      setEstaCarregando(false);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doGetEnderecoDaEscola()`
      )
    );
};

export const doPutEscola = async (
  idEscola,
  escola,
  endereco,
  localDoArquivo
) => {
  return await api
    .put(`/escolas/${idEscola}`, escola)
    .then((res) => {
      doPutEndereco(escola.enderecoId, endereco);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doPutEscola()`
      )
    );
};

export const doPutEndereco = async (enderecoId, endereco, localDoArquivo) => {
  return await api
    .put(`/enderecos/${enderecoId}`, endereco)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doPutEscola()`
      )
    );
};

export const doDeleteEscola = async (escola, localDoArquivo) => {
  escola.id &&
    (await api
      .delete(`/escolas/${escola.id}`)
      .then(() => {
        tempAlert(`Escola ${escola.nome} deletada!`, 2500);
        setTimeout(() => {
          window.location.reload();
        }, 2500);
        // refresh será resolvido com redux
      })
      .catch((error) =>
        console.log(
          "Error: " +
            error.message +
            "\n" +
            `Local: ${localDoArquivo}  -> handleDeleteEscola()`
        )
      ));
};

//-----------------------------PROFESSOR------------------------------------

export const doPostProfessor = async (professor, localDoArquivo) => {
  return await api
    .post("/professores", professor)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doPostProfessor()`
      )
    );
};

export const doPutProfessor = async (
  idDoProfessor,
  professor,
  endereco,
  localDoArquivo
) => {
  await api
    .put(`/professores/${idDoProfessor}`, professor)
    .then((res) => {
      doPutEndereco(professor.enderecoId, endereco);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doPutProfessor()`
      )
    );
};

export const doGetProfessores = async (
  termoDeBusca,
  setProfessores,
  setEstaCarregando,
  localDoArquivo
) => {
  return await api
    .get(`/professores?nome=${termoDeBusca}`)
    .then((res) => {
      setProfessores(res.data);
      setEstaCarregando(false);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doGetProfessores()`
      )
    );
};

export const doGetUmProfessor = async (
  idDoProfessor,
  setProfessor,
  setEndereco,
  setEstaCarregando,
  localDoArquivo
) => {
  return await api
    .get(`/professores/${idDoProfessor}`)
    .then((res) => {
      setProfessor(res.data);
      doGetEnderecoDoProfessor(
        res.data.enderecoId,
        setEndereco,
        setEstaCarregando
      );
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doGetUmProfessor()`
      )
    );
};

export const doGetEnderecoDoProfessor = async (
  enderecoId,
  setEndereco,
  setEstaCarregando
) => {
  return await api
    .get(`/enderecos/${enderecoId}`)
    .then((res) => {
      setEndereco(res.data);
      setEstaCarregando(false);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          "File: Pages/NRE-Professores/Professores -> doGetEnderecoDoProfessor()"
      )
    );
};

export const doDeleteProfessor = async (professorId, localDoArquivo) => {
  return await api
    .delete(`/professores/${professorId}`)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo}  -> doDeleteProfessor()`
      )
    );
};

// ------------------------------ NOTIFICAO ------------------------------

export const doGetNotificacoes = async (
  termoDeBusca,
  setNotificacoes,
  setEstaCarregando,
  idDaEntidade,
  localDoArquivo
) => {
  return await api
    .get(`/notificacoes?titulo=${termoDeBusca}&idEntidade=${idDaEntidade}`)
    .then((res) => {
      setNotificacoes(res.data);
      setEstaCarregando(false);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doGetNotificacoes()`
      )
    );
};

export const doGetHistóricoDaNotificacao = async (
  idDaNotificao,
  setHistorico,
  localDoArquivo
) => {
  return await api
    .get(`/historicos?idNotificacao=${idDaNotificao}`)
    .then((res) => {
      setHistorico(res.data);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doGetHistóricoDaNotificacao()`
      )
    );
};

export const doPutNotificacao = async (
  idDaNotificao,
  notifEditada,
  localDoArquivo
) => {
  return await api
    .put(`/notificacoes/${idDaNotificao}`, notifEditada)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doPutNotificacao()`
      )
    );
};

export const doPostNotificacao = async (novaNotificacao, localDoArquivo) => {
  return await api
    .post(`/notificacoes`, novaNotificacao)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doPostNotificacao()`
      )
    );
};

// ------------------------------ HISTORICO ------------------------------

export const doPostHistorico = async (novoHistorico, localDoArquivo) => {
  return await api
    .post(`/historicos`, novoHistorico)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doPostHistorico()`
      )
    );
};

// ------------------------------ SOLICITAÇÃO ------------------------------

export const doPostSolicitacao = async (solicitacao, localDoArquivo) => {
  return await api
    .post(`/solicitacoes`, solicitacao)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doPostSolicitacao()`
      )
    );
};

export const doGetSolicitacoes = async (
  idEscola,
  status,
  setSolicitacoes,
  localDoArquivo,
  setEstaCarregando
) => {
  return await api
    .get(`/solicitacoes?idEscola=${idEscola}&estado=${status}`)
    .then((res) => {
      setSolicitacoes(res.data);
      setEstaCarregando(false);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doGetSolicitacoes()`
      )
    );
};

export const doGetUmaSolicitacao = async (
  idSolicitacao,
  setSolicitacao,
  localDoArquivo,
  setEstaCarregando
) => {
  return await api
    .get(`/solicitacoes/${idSolicitacao}`)
    .then((res) => {
      setSolicitacao(res.data);
    })
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doGetUmSolicitacao()`
      )
    );
};

export const doPutSolicitacao = async (
  idSolicitacao,
  solicitacao,
  localDoArquivo
) => {
  return await api
    .put(`/solicitacoes/${idSolicitacao}`, solicitacao)
    .catch((error) =>
      console.log(
        "Error: " +
          error.message +
          "\n" +
          `Local: ${localDoArquivo} -> doGetUmSolicitacao()`
      )
    );
};
