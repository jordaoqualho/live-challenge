import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
 html {
    --brown: #8d6742;
    --lightBrown: #8d68421a;
    --brown_10: #8d684240;
    --off-white: #fafafa;
    --grayBorder: #373737;
    --black: rgb(0, 0, 0);
    --lightGray: #afafaf29;
    --white: #fff;
    --basicShadow:   0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    --darkGray: #828282;
    --logoFont:  'Righteous', cursive;
    --font: 'Poppins', sans-serif;
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
    text-decoration: none;
    list-style: none;
    font-family: var(--font);
    outline: none;    
  }
  ol, ul {
    padding-left: 0;
}


  button {
    cursor: pointer;
    border:none;
  }

  /* Flex Row Estilos */
  .flex_cs {
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
  }

  .flex_cb {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .flex_cc {
    display: flex;
    align-items: center;
    justify-content: center;
  }


  /* Flex Column Estilos */
  .flex_ccs {
    display: flex;
    flex-direction: column;
    align-items: start;
    justify-content: flex-start;    
  }

  .flex_ccb {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;    
  }
  
  .flex_ccc {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .grid_c {
    display: grid;
    place-items: center
  }

`;
