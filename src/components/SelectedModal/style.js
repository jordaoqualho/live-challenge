import styled from "styled-components";

export const Container = styled.div`
  background-color: var(--brown_10);
  backdrop-filter: blur(2px);
  width: 100%;
  height: 100%;
  transition: 0.5s ease;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 100;

  @media (max-width: 600px) {
    position: fixed;
  }
`;

export const Modal = styled.div`
  background-color: var(--white);
  box-shadow: var(--basicShadow);
  transition: 0.5s ease;
  position: relative;
  padding: 20px;

  img {
    height: 300px;
  }

  .span {
    text-transform: lowercase;
    color: var(--gray);
    font-weight: 200;
    margin-bottom: -5px;
    text-align: center;
    margin-top: 20px;
    font-size: 11px;
  }

  .name {
    text-align: center;
    text-transform: lowercase;
    font-size: 16px;
    font-weight: 400;
    color: var(--brown);
  }

  .btn_close {
    background-color: transparent;
    position: absolute;
    top: 0;
    right: 0;
    font-weight: 500;
    font-size: 16px;
    color: var(--brown);
    padding: 0px 10px;
    margin-bottom: 5px;
    transition: 0.3s ease;
    border-radius: 5px;
    cursor: pointer;
  }

  .sizes {
    margin-top: 10px;

    button {
      border: 1px solid var(--brown);
      margin: 0 5px;
      border-radius: 50%;
      width: 40px;
      height: 40px;
      background-color: transparent;
      color: var(--brown);
      transition: 0.3s ease;

      :hover {
        background-color: var(--brown);
        color: var(--white);
      }
    }
  }
`;
