import { tempAlert } from "functions";
import React, { useEffect } from "react";
import { Container, Modal } from "./style";
// @ts-ignore

const SelectedModal = (props) => {
  const { setSelectedModal, selectedModal } = props;

  useEffect(() => {
    handleOpen();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedModal]);

  const handleOpen = () => {
    const element = document.getElementById("modal");

    setTimeout(() => {
      if (selectedModal.open) {
        element.style.opacity = "1";
      } else {
        element.style.opacity = "0";
      }
    }, 500);
  };

  const handleClick = () => {
    tempAlert(selectedModal.product.productName.toLowerCase(), 3500);
    setSelectedModal({ ...selectedModal, open: false });
  };

  return (
    <>
      <Container className="grid_c">
        <Modal className="flex_ccc" id="modal">
          <img src={selectedModal.product.images[0]} alt="" />
          <p className="span">selecione o tamanho para</p>
          <p className="name">{selectedModal.product.productName}</p>
          <div
            className="btn_close"
            onClick={() => setSelectedModal({ ...selectedModal, open: false })}
          >
            x
          </div>
          <div className="sizes flex_cc">
            <button onClick={() => handleClick()}>PP</button>
            <button onClick={() => handleClick()}>P</button>
            <button onClick={() => handleClick()}>M</button>
            <button onClick={() => handleClick()}>G</button>
            <button onClick={() => handleClick()}>GG</button>
          </div>
        </Modal>
      </Container>
    </>
  );
};

export default SelectedModal;
