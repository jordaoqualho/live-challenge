import styled from "styled-components";

export const Container = styled.div`
  background-color: var(--white);
  transition: 0.5s ease;
  padding: 20px;
  position: absolute;
  overflow: auto;
  padding-right: 30px;
  height: 100%;
  top: 0;
  left: -500px;

  ::-webkit-scrollbar {
    width: 10px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    border-radius: 5px;
    background: transparent;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: var(--brown_10);
    border-radius: 5px;
    cursor: pointer;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: var(--brown);
  }

  .title {
    align-items: flex-start;

    p {
      font-weight: 500;
      color: var(--brown);
      margin-bottom: 10px;
    }

    .close {
      background-color: transparent;
      font-weight: 500;
      font-size: 16px;
      color: var(--brown);
      padding: 0px 10px;
      margin-bottom: 5px;
      transition: 0.3s ease;
      border-radius: 5px;

      :hover {
        background-color: var(--brown);
        color: var(--white);
      }
    }
  }

  .search_bar {
    input {
      background-color: var(--lightBrown);
      color: var(--brown);
      font-weight: 400;
      border-radius: 5px;
      font-size: 14px;
      padding: 10px;
      border: none;
      width: 400px;
    }
  }

  @media (max-width: 600px) {
    position: relative;
    left: 0 !important;
    top: 0;
    opacity: 1 !important;

    .title {
      .close {
        display: none;
      }
    }

    .search_bar {
      input {
        width: 100%;
      }
    }
  }
`;

export const List = styled.div`
  margin-left: -15px;
  padding-top: 20px;

  @media (max-width: 600px) {
    display: flex;
    margin-left: 0;
    flex-wrap: wrap;
  }
`;

export const Product = styled.div`
  margin-bottom: 25px;
  width: 220px;
  height: 283px;

  img {
    width: 100%;
  }

  .info {
    padding: 5px;
    height: 100%;
    width: 100%;

    .name {
      font-size: 14px;
      width: 200px;
      text-transform: lowercase;
      color: var(--brown);
    }

    .price {
      color: var(--brown);
      font-weight: 500;
    }

    .part_price {
      color: var(--brown);
      font-weight: 400;
      margin-top: -5px;
      font-size: 10px;
    }

    .description {
      font-size: 10px;
      margin-top: 10px;
      text-transform: lowercase;
      width: 200px;
      color: var(--brown);
      height: 100px;
      overflow: auto;

      ::-webkit-scrollbar {
        width: 6px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
        border-radius: 5px;
        background: var(--lightBrown);
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
        background: var(--brown);
        border-radius: 5px;
        cursor: pointer;
      }
    }

    .add_btn {
      background-color: var(--brown);
      width: 100%;
      padding: 10px;
      color: var(--white);
      transition: 0.3s ease;

      :hover {
        opacity: 0.5;
      }
    }
  }

  @media (max-width: 600px) {
    height: fit-content;
    flex-direction: column;
    width: 50%;

    .info {
      .name {
        width: 100%;
      }

      .description {
        width: 150px !important;
        overflow: auto !important;
      }
    }
  }
`;
