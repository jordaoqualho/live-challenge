import React, { useState, useEffect } from "react";
import { Container, List, Product } from "./style";
// @ts-ignore
import { products } from "data/mock.json";
import SelectedModal from "components/SelectedModal";

const ShopList = (props) => {
  const { setOpenShopList, openShopList } = props;
  const [liveProd, setLiveProd] = useState([]);
  const [search, setSearch] = useState("");
  const [selectedModal, setSelectedModal] = useState({
    open: false,
    product: {},
  });
  const [list, setList] = useState([]);

  useEffect(() => {
    doGetHelp(search);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  useEffect(() => {
    handleOpen();
    getAll();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [openShopList]);

  const getAll = () => {
    const tempArray = [];
    for (const prod of products) {
      tempArray.push(prod);
    }
    setLiveProd([...tempArray]);
    setList([...tempArray]);
    console.log(tempArray);
  };

  const handleOpen = () => {
    const element = document.getElementById("shop_list");

    if (openShopList) {
      element.style.left = "0";
      element.style.opacity = "1";
    } else {
      element.style.left = "-500px";
      element.style.opacity = "0";
    }
  };

  const doGetHelp = (termo) => {
    let arrayX = new Array(liveProd.length);
    if (termo === "") {
      setList(liveProd);
    } else {
      for (let i = 0; i < liveProd.length; i++) {
        let element = liveProd[i] ? liveProd[i].productName.toLowerCase() : "";
        if (element.indexOf(termo.toLowerCase()) > -1) {
          arrayX[i] = liveProd[i];
        }
      }
      setList(arrayX);
    }
  };

  const handleInputChange = async (event) => {
    setSearch(event.target.value);
  };

  return (
    <>
      <Container id="shop_list">
        <div className="title flex_cb">
          <p>produtos</p>
          <button className="close" onClick={() => setOpenShopList(false)}>
            x
          </button>
        </div>
        <div className="search_bar">
          <input
            type="text"
            placeholder="Digite aqui para pesquisar um produto..."
            onChange={handleInputChange}
          />
        </div>
        <List>
          {list.map((prod, index) => (
            <Product key={index} className="flex_cs">
              <img src={prod.images[0]} alt="" />
              <div className="info flex_ccb">
                <p className="name">{prod.productName}</p>
                <p className="price">R$ {prod.price.toFixed(2)}</p>
                <p className="part_price">
                  ou em 10x de R$ {(prod.price / 10).toFixed(2)}
                </p>
                <p className="description">
                  {prod.description[0] && prod.description[0].content}
                </p>
                <button
                  className="add_btn"
                  onClick={() =>
                    setSelectedModal({ open: true, product: prod })
                  }
                >
                  incluir na mochila
                </button>
              </div>
            </Product>
          ))}
        </List>
      </Container>
      {selectedModal.open && (
        <SelectedModal
          selectedModal={selectedModal}
          setSelectedModal={setSelectedModal}
        />
      )}
    </>
  );
};

export default ShopList;
